__version__ = '1.0.5'

class Plugin(object):
    def get_before_urls(self):
        from . import urls
        return urls.urlpatterns

    def get_apps(self):
        return [__name__]

    def get_authentication_backends(self):
        return ['authentic2_auth_msp.backends.MspBackend']

    def get_auth_frontends(self):
        return ['authentic2_auth_msp.auth_frontends.MspFrontend']
