from . import models

from django.contrib.auth.backends import ModelBackend


class MspBackend(ModelBackend):
    def authenticate(self, agc=None, **kwargs):
        try:
            msp_account = models.MspAccount.objects.get(agc=agc)
            return msp_account.user
        except models.MspAccount.DoesNotExist:
            pass

    def get_saml2_authn_context(self):
        import lasso
        return lasso.SAML2_AUTHN_CONTEXT_PASSWORD_PROTECTED_TRANSPORT
