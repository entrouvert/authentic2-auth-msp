from django.conf.urls import patterns, url, include

from . import views

msppatterns = patterns('',
    url(r'^login/$', views.login, name='msp-login'),
    url(r'^link/$', views.link, name='msp-link'),
    url(r'^login-or-link/$', views.login_or_link, name='msp-login-or-link'),
    url(r'^link-management/$', views.link_management,
        name='msp-link-management'),
    url(r'^link-management/unlink/confirm/$', views.confirm_unlink,
        name='msp-confirm-unlink'),
    url(r'^link-management/unlink/done/$', views.unlink_done,
        name='msp-unlink-done'),
    url(r'^link-management/unlink/$', views.unlink, name='msp-unlink'),
    url(r'^authorize/$', views.authorize, name='msp-authorize'),
    url(r'^access_token/$', views.access_token, name='msp-access-token'),
    url(r'^documents/$', views.documents, name='msp-documents'),
    url(r'^documents/(?P<doc_id>[^/]*)/$', views.document,
        name='msp-document'),
    url(r'^more/$', views.more_redirect, name='msp-more-redirect'),
)

urlpatterns = patterns('',
        url(r'^msp/', include(msppatterns)),
)
