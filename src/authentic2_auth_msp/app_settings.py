
class AppSettings(object):
    '''Thanks django-allauth'''
    __SENTINEL = object()

    def __init__(self, prefix):
        self.prefix = prefix

    def _setting(self, name, dflt=__SENTINEL):
        from django.conf import settings
        from django.core.exceptions import ImproperlyConfigured

        v  = getattr(settings, self.prefix + name, dflt)
        if v is self.__SENTINEL:
            raise ImproperlyConfigured('Missing setting %r' % (self.prefix + name))
        return v

    @property
    def enabled(self):
        return self._setting('ENABLED', False)

    @property
    def authorize_url(self):
        return self._setting('AUTHORIZE_URL', 'https://mon.service-public.fr/apis/app/oauth/authorize')

    @property
    def token_url(self):
        return self._setting('TOKEN_URL', 'https://mon.service-public.fr:2443/apis/app/oauth/token')

    @property
    def api_url(self):
        return self._setting('API_URL', 'https://mon.service-public.fr:2443/apis/')

    @property
    def client_id(self):
        return self._setting('CLIENT_ID')

    @property
    def client_secret(self):
        return self._setting('CLIENT_SECRET')

    @property
    def client_certificate(self):
        return self._setting('CLIENT_CERTIFICATE', None)

    @property
    def verify_certificate(self):
        return self._setting('VERIFY_CERTIFICATE', False)

    @property
    def client_credentials(self):
        return self._setting('CLIENT_CREDENTIALS', ())

    @property
    def more_url(self):
        return self._setting('MORE_URL', 'https://mon.service-public.fr/')


import sys

app_settings = AppSettings('A2_MSP_')
app_settings.__name__ = __name__
sys.modules[__name__] = app_settings
