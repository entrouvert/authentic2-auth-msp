from django.utils.translation import gettext_noop
from django.template.loader import render_to_string
from django.template import RequestContext
from django.shortcuts import render

from . import app_settings

class MspFrontend(object):
    def enabled(self):
        return app_settings.enabled

    def name(self):
        return gettext_noop('mon.service-public.fr')

    def id(self):
        return 'msp'

    def login(self, request, *args, **kwargs):
        if 'nomsp' in request.GET:
            return
        context_instance = kwargs.pop('context_instance', None)
        return render(request, 'authentic2_auth_msp/login.html',
                context_instance=context_instance)

    def profile(self, request, *args, **kwargs):
        context_instance = kwargs.pop('context_instance', None) or RequestContext(request)
        return render_to_string('authentic2_auth_msp/linking.html', {'popup': True},
                context_instance=context_instance)

